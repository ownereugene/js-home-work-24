1. Теоретический вопрос
Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

Конструкция try...catch используется в коде для обработки исключительных ситуаций, которые могут возникнуть во время выполнения программы. 
Работа с файлами: при чтении или записи файла могут возникнуть ошибки, например, если файл не уществует или нет доступа к нему. В этом случае можно использовать конструкцию try...catch для обработки исключения и вывода соответствующего сообщения пользователю