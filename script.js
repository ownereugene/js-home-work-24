let game = document.querySelector(".game");
const countFlagEl = document.querySelector("p");
const newGame = document.querySelector(".startNewGame");
newGame.addEventListener("click", () => {
  window.location.reload();
});
localStorage.setItem("countFlag", 0);
const writeFlagCount = (action = "first") => {
    console.log(action)
    let countFlags = parseInt(localStorage.getItem("countFlag"))
    if (action === "first") {
       countFlags = 0
    }
    if (action === "minus") {
       countFlags -= 1
    }
    if (action === "plus") {
        countFlags += 1
        
        //countFlag = localStorage.getItem("countFlag")
    }
    localStorage.setItem("countFlag", countFlags)
    countFlagEl.innerHTML = `Count flag: ${countFlags}/10`
};
writeFlagCount()
let currentNumber = 0;
let arrBoom = [];
for (i = 0; i <= 9; i++) {
  let random = Math.round(Math.random() * 63);
  arrBoom.forEach((el) => {
    if (el == random) {
      if (random == 0) {
        random += 1;
      } else {
        random -= 1;
      }
    }
  });
  arrBoom.push(random);
}
console.log(arrBoom);
for (let i = 1; i <= 8; i++) {
  game.innerHTML += `<div class="game-row" row="${i}"></div>`;
  let currentRow = document.querySelector(`div[row="${i}"]`);
  for (let j = 1; j <= 8; j++) {
    let isBomb = false;
    arrBoom.forEach((el) => {
      if (el == currentNumber) {
        isBomb = true;
      }
    });
    currentRow.innerHTML += `
        <div class="game-item" isBomb="${isBomb}" isOpen="false" colNumber="${currentNumber}">
            <p>${currentNumber + 1}</p>
        </div>`;
    currentNumber += 1;
  }
}
const gameItems = document.querySelectorAll(".game-item");

const gameOver = () => {
  gameItems.forEach((gameItem) => {
    if (gameItem.getAttribute("isBomb") == "true") {
      gameItem.innerHTML = `<img class="icon" src="./bomb.svg">`;
    }
  });
  setTimeout(() => {
    game.innerHTML = ``;
    alert("Game over");
    window.location.reload();
  }, 3000);
};

const writeCountBombOnRow = (countBomb, colNumber) => {
  gameItems[colNumber].setAttribute("isOpen","true");
  gameItems[colNumber].innerHTML = countBomb;
  gameItems[colNumber].style.background = "green";
};

const getCountBomb = (colNumber, rowIndex) => {
  let countBombOnRow = 0;
  gameItems.forEach((gameItem) => {
    if (gameItem.parentNode.getAttribute("row") == rowIndex) {
      if (gameItem.getAttribute("isBomb") == "true") {
        countBombOnRow += 1;
      }
    }
  });
  writeCountBombOnRow(countBombOnRow, colNumber);
};

gameItems.forEach((item) => {
  item.addEventListener("click", () => {
    newGame.style.display = "block";
    const itemValue = {
      isBomb: item.getAttribute("isBomb"),
      number: item.getAttribute("colNumber"),
    };
    const row = item.parentNode;
    const rowIndex = row.getAttribute("row");

    if (itemValue.isBomb == "true") {
      gameOver();
    } else {
      getCountBomb(itemValue.number, rowIndex);
    }
  });
  item.addEventListener("contextmenu", (e) => {
    e.preventDefault();
    const isOpen = item.getAttribute("isOpen") 
    if (item.getAttribute("isFlag")== "true") {
        writeFlagCount('minus')
      item.innerHTML = "";
      item.setAttribute("isFlag", "false");
    } else {
      let countFlags = parseInt(localStorage.getItem("countFlag"))
      if (countFlags < 10 && isOpen === "false"){
        writeFlagCount('plus')
        item.innerHTML = `<img class="icon" src="./flag.svg">`;
        item.setAttribute("isFlag", "true");
      }
    }
  });
});